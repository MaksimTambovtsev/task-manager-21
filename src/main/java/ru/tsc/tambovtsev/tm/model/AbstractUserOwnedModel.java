package ru.tsc.tambovtsev.tm.model;

public abstract class AbstractUserOwnedModel extends AbstractEntity {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}

