package ru.tsc.tambovtsev.tm.service;

import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IUserService;
import ru.tsc.tambovtsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import javax.jws.soap.SOAPBinding;
import java.util.List;
import java.util.Optional;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    public UserService(IUserRepository repository) {
        super(repository);
    }

    @Override
    public User findByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(() -> new LoginEmptyException());
        return repository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) {
        Optional.ofNullable(email).orElseThrow(() -> new EmailEmptyException());
        return repository.findByEmail(email);
    }

    @Override
    public User removeByLogin(final String login) {
        Optional.ofNullable(login).orElseThrow(() -> new LoginEmptyException());
        return repository.removeByLogin(login);
    }

    @Override
    public boolean isLoginExists(final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public User create(final String login, final String password) {
        Optional.ofNullable(login).orElseThrow(() -> new LoginEmptyException());
        Optional.ofNullable(password).orElseThrow(() -> new PasswordEmptyException());
        if (isLoginExists(login)) throw new LoginExistsException();
        final User user = new User();
        user.setRole(Role.USUAL);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        Optional.ofNullable(login).orElseThrow(() -> new LoginEmptyException());
        Optional.ofNullable(password).orElseThrow(() -> new PasswordEmptyException());
        Optional.ofNullable(email).orElseThrow(() -> new EmailEmptyException());
        if (isEmailExists(email)) throw new EmailExistsException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        Optional.ofNullable(login).orElseThrow(() -> new LoginEmptyException());
        Optional.ofNullable(password).orElseThrow(() -> new PasswordEmptyException());
        Optional.ofNullable(role).orElseThrow(() -> new RoleEmptyException());
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return repository.add(user);
    }

    @Override
    public User setPassword(final String userId, final String password) {
        Optional.ofNullable(userId).orElseThrow(() -> new UserNotFoundException());
        Optional.ofNullable(password).orElseThrow(() -> new PasswordEmptyException());
        final User user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
        return user;
    }

    @Override
    public User updateUser(final String userId, final String firstName, final String lastName, final String middleName) {
        Optional.ofNullable(userId).orElseThrow(() -> new UserNotFoundException());
        final User user = findById(userId);
        Optional.ofNullable(firstName).orElseThrow(() -> new NameEmptyException());
        Optional.ofNullable(lastName).orElseThrow(() -> new NameEmptyException());
        Optional.ofNullable(middleName).orElseThrow(() -> new NameEmptyException());
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return null;
    }

}
