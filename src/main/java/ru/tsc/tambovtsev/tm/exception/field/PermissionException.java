package ru.tsc.tambovtsev.tm.exception.field;

public final class PermissionException extends AbstractMethodError {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
