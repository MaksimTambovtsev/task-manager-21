package ru.tsc.tambovtsev.tm.component;

import ru.tsc.tambovtsev.tm.api.controller.ICommandController;
import ru.tsc.tambovtsev.tm.api.controller.IProjectController;
import ru.tsc.tambovtsev.tm.api.controller.IProjectTaskController;
import ru.tsc.tambovtsev.tm.api.controller.ITaskController;
import ru.tsc.tambovtsev.tm.api.repository.ICommandRepository;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.*;
import ru.tsc.tambovtsev.tm.command.AbstractCommand;
import ru.tsc.tambovtsev.tm.command.project.*;
import ru.tsc.tambovtsev.tm.command.system.*;
import ru.tsc.tambovtsev.tm.command.task.*;
import ru.tsc.tambovtsev.tm.command.user.*;
import ru.tsc.tambovtsev.tm.constant.ArgumentConst;
import ru.tsc.tambovtsev.tm.constant.TerminalConst;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.tambovtsev.tm.exception.system.CommandNotSupportedException;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.repository.CommandRepository;
import ru.tsc.tambovtsev.tm.repository.ProjectRepository;
import ru.tsc.tambovtsev.tm.repository.TaskRepository;
import ru.tsc.tambovtsev.tm.repository.UserRepository;
import ru.tsc.tambovtsev.tm.service.*;
import ru.tsc.tambovtsev.tm.util.DateUtil;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

import javax.xml.crypto.Data;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Scanner;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);


    {
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new HelpCommand());
        registry(new AboutCommand());
        registry(new ExitCommand());
        registry(new ArgListCommand());
        registry(new CmdListCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskShowListByProjectIdCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectListCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCascadeRemoveCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindToProjectCommand());
        registry(new UserRegistrationCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserShowProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());

    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }


    public void run(final String[] args) {
        if (processArgument(args)) System.exit(0);
        initUsers();
        initData();
        initLogger();
        String command = "";
        while (!ExitCommand.NAME.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void initData() {
        taskService.create("DEMO TASK", "Simple task...");
        taskService.create("TEST TASK", "Simple task...");
        taskService.create("MEGA TASK", "Simple task...");
        projectService.add(new Project("DEMO PROJECT", Status.NOT_STARTED, DateUtil.toDate("04.10.2019")));
        projectService.add(new Project("TEST PROJECT", Status.IN_PROGRESS, DateUtil.toDate("05.03.2018")));
        projectService.add(new Project("MEGA PROJECT", Status.IN_PROGRESS, DateUtil.toDate("16.02.2020")));
        projectService.add(new Project("BEST PROJECT", Status.COMPLETED, DateUtil.toDate("22.01.2021")));
    }

    private void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void close() {
        System.exit(0);
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }


    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }


}