package ru.tsc.tambovtsev.tm.repository;

import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User removeUser(final User user) {
        models.remove(user);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        return models.stream()
                .filter(item -> login.equals(item.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return models.stream()
                .filter(item -> email.equals(item.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

}

