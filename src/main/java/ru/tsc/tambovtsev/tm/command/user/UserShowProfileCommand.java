package ru.tsc.tambovtsev.tm.command.user;

import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;

public final class UserShowProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-show-profile";

    public static final String DESCRIPTION = "Show user profile.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER SHOW PROFILE]");
        final User user = getAuthService().getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
