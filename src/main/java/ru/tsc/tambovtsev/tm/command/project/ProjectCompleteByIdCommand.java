package ru.tsc.tambovtsev.tm.command.project;

import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-complete-by-id";

    public static final String DESCRIPTION = "Complete project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
        project.setDateEnd(new Date());
    }

}
