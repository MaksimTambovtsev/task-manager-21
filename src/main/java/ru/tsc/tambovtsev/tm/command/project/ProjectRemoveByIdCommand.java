package ru.tsc.tambovtsev.tm.command.project;

import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-remove-by-id";

    public static final String DESCRIPTION = "Remove project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findById(userId, id);
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

}
