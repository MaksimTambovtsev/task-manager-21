package ru.tsc.tambovtsev.tm.api.repository;

import ru.tsc.tambovtsev.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    AbstractCommand getCommandByArgument(String argument);

    AbstractCommand getCommandByName(String name);

    Collection<AbstractCommand> getCommands();

}
