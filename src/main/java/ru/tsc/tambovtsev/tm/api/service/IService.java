package ru.tsc.tambovtsev.tm.api.service;

import ru.tsc.tambovtsev.tm.api.repository.IRepository;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractEntity> {

    M add(M model);

    List<M> findAll();

    M remove(M model);

    void clear();

    M findById(String id);

    M removeById(String id);

    List<M> findAll(Comparator comparator);

    List<M> findAll(Sort sort);

    void removeAll(Collection<M> collection);

    int getSize();

    boolean existsById(String id);

}

